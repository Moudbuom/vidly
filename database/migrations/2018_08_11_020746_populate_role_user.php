<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateRoleUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::statement('insert into role_user (user_id, role_id) values (9, 1)');
        // DB::statement('insert into role_user (user_id, role_id) values (8, 2)');
        // DB::statement('insert into role_user (user_id, role_id) values (10, 3)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement('delete from role_user where user_id=10');
        // DB::statement('delete from role_user where user_id=8');
        // DB::statement('delete from role_user where user_id=9');
    }
}
