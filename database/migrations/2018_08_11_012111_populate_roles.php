<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('insert into roles (title) values ("Manager")');
        DB::statement('insert into roles (title) values ("Employee")');
        DB::statement('insert into roles (title) values ("Customer")');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('delete from roles where title="Manager"');
        DB::statement('delete from roles where title="Employee"');
        DB::statement('delete from roles where title="Customer"');
    }
}
