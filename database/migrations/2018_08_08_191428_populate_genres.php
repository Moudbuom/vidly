<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('Insert into genres (name) values ("Drama")');
        DB::statement('Insert into genres (name) values ("Comedy")');
        DB::statement('Insert into genres (name) values ("Thriller")');
        DB::statement('Insert into genres (name) values ("Action")');
        DB::statement('Insert into genres (name) values ("Fiction")');
        DB::statement('Insert into genres (name) values ("Romance")');
        DB::statement('Insert into genres (name) values ("Romantic Comedy")');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('delete from genres where name="Romantic Comedy"');
        DB::statement('delete from genres where name="Romance"');
        DB::statement('delete from genres where name="Fiction"');
        DB::statement('delete from genres where name="Action"');
        DB::statement('delete from genres where name="Thriller"');
        DB::statement('delete from genres where name="Comedy"');
        DB::statement('delete from genres where name="Drama"');
    }
}
