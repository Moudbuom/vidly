<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    $number_of_genres = App\Genre::all()->count();
    return [
        'name' => $faker->unique()->text(15),
        'genre_id' => $faker->numberBetween($min = 1, $max = $number_of_genres),
        'number_in_stock' => $faker->numberBetween($min = 1, $max = 20),
        'release_date' =>
        $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null)->format('Y-m-d')
    ];
});
