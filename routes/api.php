<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::get('/user/verify/{token}', 'UserController@verifyUser');

Route::group(['middleware' => ['auth:api', 'App\Http\Middleware\UserIsActivated']], function(){
    Route::post('/logout', 'UserController@logout');
    Route::get('details', 'UserController@details');


    Route::get('movies', 'MoviesController@index');
    Route::post('movie', 'MoviesController@store')->middleware('App\Http\Middleware\CanEditMovies');

    Route::put('movie', 'MoviesController@store')->middleware('App\Http\Middleware\CanEditMovies');

    Route::delete('movie/{id}', 'MoviesController@destroy');

    Route::post('add/user/manager', 'DeveloperDashboardController@createManager')->middleware('App\Http\Middleware\IsDevPrivilege');

   

    // * Users register themselves
    //  * Manager can view and delete them
    Route::get('employees', 'StaffController@index')->middleware('App\Http\Middleware\CanEditStaffMembers');

    Route::put('employee', 'StaffController@store')->middleware('App\Http\Middleware\CanEditStaffMembers');

    Route::delete('employee/{id}', 'StaffController@destroy')->middleware('App\Http\Middleware\CanEditStaffMembers');
});