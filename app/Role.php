<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    public $table = "roles";

    public static function developerId() {
        return Role::where('title' , '=', 'Developer')->first()->id;
    }
    public static function managerId() {
        return Role::where('title' , '=', 'Manager')->first()->id;
    }
    public static function employeeId() {
        return Role::where('title' , '=', 'Employee')->first()->id;
    }
}
