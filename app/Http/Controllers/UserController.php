<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\UserManager;
Use App\VerifyUser;
Use App\Role;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use Validator;


class UserController extends Controller 
{
public $successStatus = 200;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function verifyUser($token)
    {
      $verifyUser = VerifyUser::where('token', $token)->first();
      if(isset($verifyUser) ){
        $user = $verifyUser->user;
        if(!$user->verified) {
          $verifyUser->user->verified = 1;
          $verifyUser->user->save();
          $status = "Your e-mail is verified. You can now login.";
        }else{
          $status = "Your e-mail is already verified. You can now login.";
        }
      }else{
        return response()->json(['warning'=>"Sorry your email cannot be identified."], 404);
      }
      return response()->json(['success'=> $user, 'status' => $status], $this-> successStatus);
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        $success['name'] =  $user->name;

        // Request activation
        $verifyUser = VerifyUser::create([
          'user_id' => $user->id,
          'token' => str_random(40)
        ]);

        // TODO: Implement this kind of logic in seperate class
        // Check if the developer is registering 
        // Consider ignoring letter cases
        if($input['email'] === "baibars@developer.dev") 
        {
            $user->role()->save(Role::find(Role::developerId()));
            // Developers do not require email verification (verified automatically)
            $this->verifyUser($user->verifyUser->token);
        } else if (UserManager::all()->contains('email', $input['email']) ) {
            $user->role()->save(Role::find(Role::managerId()));
        }
        // -------

        error_log("token url is : ".url('api/user/verify', $user->verifyUser->token));

        return response()->json(['success'=>$success], $this-> successStatus); 
    }


    public function logout()
    {
        $accessToken = auth()->user()->token();
        error_log($accessToken);

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
        ]);
        error_log($refreshToken);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }

/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $userWithRole = User::with('role')->get()->find(Auth::user()->id);  // Include role
        return response()->json(['success' => $userWithRole], $this-> successStatus); 
    } 
}