<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserManager;
use App\Role;
use App\Http\Resources\UserManager as UserManagerResource;

class DeveloperDashboardController extends Controller
{
    /**
     * create a newly created user resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createManager(Request $request)
    {
        $manager = new UserManager;
        $manager->email = $request->input('email');

        if($manager->save()) {
            return new UserManagerResource($manager);
        }
    }
}
