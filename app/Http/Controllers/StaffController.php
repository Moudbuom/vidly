<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Resources\StaffMember as StaffMemberResource;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffMembers = User::all()->map(function($user) {
            $userWithRole['name'] = $user->name;
            $userWithRole['email'] = $user->email;
            // TODO: condition assignment only if user is having a role 
            $userWithRole['role'] = Role::find($user->role->first()->pivot->role_id)->title;
            return $userWithRole;
        });
        return StaffMemberResource::collection($staffMembers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $role = Role::find($request->input('roleId'));

        if($user->role()->save($role)) {
            $userWithRole = [
                'name'=> $user->name, 'email'=> $user->email, 'role'=> $user->role->first()->pivot->role_id
            ];
            return new StaffMemberResource($userWithRole);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if($user->role->first()->pivot->delete()) {
            if($user->delete()) {
                return new StaffMemberResource($user); 
            }  
        }

    }
}
