<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use Illuminate\Support\Facades\Auth; 


class CanEditStaffMembers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role[0]->pivot->role_id != Role::managerId()) {
            // TODO: send unauthorized user json resource
            return redirect('home');
        }
        return $next($request);
    }
}
