<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use Illuminate\Support\Facades\Auth; 


class CanEditMovies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role[0]->pivot->role_id != Role::managerId() 
        && Auth::user()->role[0]->pivot->role_id != Role::employeeId())
        {
            // TODO: send unauthorized user json resource
            return response()->json(['error'=>'You are unauthorized for this page'], 401);
        }
        return $next($request);
  
    }
}
