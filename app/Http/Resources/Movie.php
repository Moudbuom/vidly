<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Movie extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'releaseDate' => $this->release_date,
            'genreId' => $this->genre_id,
            'numberInStock' => $this->number_in_stock,
            'createdAt' => $this->created_at
        ];
    }
}
