
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueRouter from 'vue-router';
import router from './routes.js';
import auth from './auth.js';

require('./bootstrap');


window.auth = auth;
window.Vue = require('vue');
window.Event = new Vue;



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter);

Vue.component('app-component', require('./components/AppComponent.vue'));
Vue.component('home-component', require('./components/HomeComponent.vue'));
Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.component('movies-component-can-view', require('./components/movies/CanViewMovies.vue'));
Vue.component('movies-component-can-edit', require('./components/movies/CanEditMovies.vue'));

Vue.component('users-component', require('./components/UsersListComponent.vue'));


Vue.component('header-component', require('./components/header/HeaderComponent.vue'));
Vue.component('login-component', require('./components/LoginComponent.vue'));
Vue.component('logout-component', require('./components/LogoutComponent.vue'));

Vue.component('controlpanel-component', require('./components/ControlpanelComponent.vue'));






const app = new Vue({
    el: '#app',
    router
});
