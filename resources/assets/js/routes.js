import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: require('./components/HomeComponent.vue')
    },                
    {
        path: '/control',
        component: require('./components/ControlpanelComponent.vue'),
        children: [
            {
              // UserProfile will be rendered inside User's <router-view>
              // when /user/:id/profile is matched
              path: 'users',
              component: require('./components/UsersListComponent.vue')
            },
            {
                path: 'movies',
                component: require('./components/movies/CanEditMovies.vue'),
                meta: { requiresCanEditMovies: true}
            }
          ]
    },
    {
        path: '/movies',
        component: require('./components/movies/CanViewMovies.vue')
    }
];

const router = new VueRouter({
    routes
});


router.beforeEach((to, from, next) => {
    if(to.path === "/movies") {
        if (auth.token == null) {
            // user no even logged in
            next()
        } else {
            auth.checkIfCanEditMovies((canEditMovies) => {
                if(canEditMovies) {
                    next({'path': '/editor/movies'});
                } else {
                    next();
                }
            });
        }

    }
    // else if(to.path === "/users") {
    //     auth.fetchRoleOfUser((userRole) => {
    //         if(userRole === "Manager") {
    //             next({'path': '/users'});
    //         } else {
    //             next({'path': '/'});
    //         }
    //     });
    // }
    else {
        // other routes
        next();
    }
  })

export default router

