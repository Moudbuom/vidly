class Auth {
  constructor() {
    this.token = window.localStorage.getItem('token') || null;
  }

  check () {
    return !! this.token;
  }
  forgetToken() {
    this.token = null;
    window.localStorage.removeItem('token')
  }

  login (token) {    
    window.localStorage.setItem('token', token);

    axios.defaults.headers.common['Authorization'] = token;

    this.token = token;
    Event.$emit('userLoggedIn');
  }

  fetchRoleOfUser(next) {
    fetch("/api/details", {
      method: "get", 
      headers: {
          'Authorization': window.localStorage.getItem('token'),
          'Accept': "application/json",
          'Content-Type': "application/json"
      }
    })
    .then(res => res.json())
    .then(res => {
      next(res.success.role[0].title)
    })
    .catch(err => console.log(err));
  }

  checkIfCanEditMovies(next) {
    fetch("/api/details", {
      method: "get", 
      headers: {
          'Authorization': window.localStorage.getItem('token'),
          'Accept': "application/json",
          'Content-Type': "application/json"
      }
    })
    .then(res => res.json())
    .then(res => {
      let role  = res.success.role[0].title;
      if (role != "Customer") {
        next(true);
      }
      next(false);
    })
    .catch(err => console.log(err));
  }

}

export default new Auth();